﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyzableCodes.Helpers
{
    class Validator
    {

        public static bool PasswordChecker(string password)
        {
            return !string.IsNullOrEmpty(password) && password.Length >= 8;
        }

        public static bool ExitWatcher(string command)
        {
            return command != "bye" || command != "quit";
        }
    }
}
